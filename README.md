Small helper to produce html formated document from markdown formating
======================================================================


Start a new documentation
-------------------------

To start a new documentation:

```
 $ git clone git@github.com:herbert-de-vaucanson/trame_owi.git my_doc
 $ cd my_project
 $ ./sh/starts.sh
 $ #enter you project name (no space)
 $ ````doc doc doc````
 $ make my_doc
```

Your html file will be available in out/


Use custom style.css, header.html, footer.html
----------------------------------------------

You can overide css/style.css html/header.html html/footer.html
default style, header and footer by placing inside your
src/project_name some files with the same path and name.

For instance a project organized as follow:

```
src/
└── example
    ├── css
    │   └── style.css
    ├── html
    │   └── header.html
    ├── img
    │   └── example-image.png
    └── main.md
```
will overide the default style.css and header.html bu will keep using
the default footer.html
