#!/bin/bash

PROJECT="$1"
SRC_DIR="src"
TITLE_DIR="titles"
TRAME="example"

usage()
{
    echo "Usage: $0 [Project_name]"
    echo "Start a new documentation project, if no name is specified"
    echo "then the interractive mode will be used."
    exit 1
}

error()
{
    echo "Error: $@"
    exit 2
}

sanitize()
{
    PROJECT=`echo $PROJECT | tr ' ' '_' | tr -d -c [:alnum:]_`
    if [ "x$PROJECT" = "x" ]
    then
	error "project name should contain only alphanumeric chars"
    fi
}

init_project()
{
    sanitize
    cp -r $SRC_DIR/$TRAME $SRC_DIR/$PROJECT/
    cp $TITLE_DIR/$TRAME.txt $TITLE_DIR/$PROJECT.txt
    echo "done"
    exit 0
}

interactive()
{
    echo -n "Project name: "
    read PROJECT
    sanitize
    init_project
}

if [ "x$PROJECT" = "x" ]
then
    interactive
else
    init_project
fi
