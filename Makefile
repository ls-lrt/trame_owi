# General
SRC_DIR   = src
TITLE_DIR = titles
OUT_DIR   = out

# Html specific
DEF_HEADER = html/header.html
DEF_FOOTER = html/footer.html
DEF_STYLE  = css/style.css

# Guess all target by scanning $(SRC_DIR)
DIRS = $(patsubst $(SRC_DIR)/%,%,\
	$(shell find $(SRC_DIR)/ -maxdepth 1 -type d))

SRC = $(DIRS)

## Set the variable $(1) to $(2)/$(3) if it exists otherwise set it to
# the default value $(3)
#
# $(1): OUT: variable to set
# $(2): IN    : project to look in
# $(3): IN    : file to look for
#
define var_init =
ifneq (,$(wildcard $(2)/$(3)))
$$(info found $(2)/$(3))
$$(eval $(1) := $(2)/$(3))
else
$$(eval $(1) := $(3))
endif
endef

## Create an new target $(1), this target should be a valid path under
# $(SRC_DIR)
#
# $(1): IN: name of the target
#
define create_target =
# use files found in $(SRC_DIR)/$(1) if they exist, otherwise use
# default
$(eval $(call var_init,HEADER,$(SRC_DIR)/$(1),$(DEF_HEADER)))
$(eval $(call var_init,FOOTER,$(SRC_DIR)/$(1),$(DEF_FOOTER)))
$(eval $(call var_init,STYLE,$(SRC_DIR)/$(1),$(DEF_STYLE)))

.PHONY: $(1)
$(1): $(SRC_DIR)/$(1)/main.md $(TITLE_DIR)/$(1).txt
	$(shell [ ! -d $(OUT_DIR) ] && mkdir $(OUT_DIR))
	@echo "Generating $(OUT_DIR)/$(1).html..."
	pandoc --toc -s --self-contained -c $(STYLE) -B $(HEADER) -A $(FOOTER) -S -o $(OUT_DIR)/$$@.html $(TITLE_DIR)/$$@.txt $(SRC_DIR)/$$@/main.md
endef

# Create one target for each $(SRC) element
$(foreach i, $(SRC), $(eval $(call create_target,$(i))))

DEFAULT_GOAL := all
all: $(SRC)

test:
	@echo $(DIRS)

mr-proper:
	rm -rf out/*
